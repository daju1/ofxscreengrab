#pragma once

#include "ofMain.h"
#include "ofxScreenGrab.h"

class ofxScreenGrabCropped {
    int screen_w;
    int screen_h;
    float grab_x;
    float grab_y;
    float draw_br_x;
    float draw_br_y;

public:
    void setup(int width, int height, bool retina) {
        this->width = width;
        this->height = height;
        this->retina = retina;
        grabber.setup(ofGetScreenWidth(), ofGetScreenHeight(), retina);
        tl.set(0, 0);
        br.set(width, height);
        mouseRadius = 10;
        debug = false;
    }
    
    void update() {
        screen_w = ofGetScreenWidth();
        screen_h = ofGetScreenHeight();
        if (debug) {
            grabber.grabScreen(0, 0, screen_w, screen_h);
            grabber.getTextureReference().readToPixels(pixels);
            cropped.setFromPixels(pixels);
            cropped.crop(ofMap(tl.x, 0, width, 0, grabber.getWidth()),
                         ofMap(tl.y, 0, height, 0, grabber.getHeight()),
                         ofMap(br.x-tl.x, 0, width, 0, grabber.getWidth()),
                         ofMap(br.y-tl.y, 0, height, 0, grabber.getHeight()));
        }
        else {
            grab_x = ofMap(tl.x, 0, width, 0, screen_w);
            grab_y = ofMap(tl.y, 0, height, 0, screen_h);
            draw_br_x = ofMap(br.x , 0, width, 0, screen_w);
            draw_br_y = ofMap(br.y, 0, height, 0, screen_h);
            grabber.grabScreen(grab_x, grab_y, draw_br_x - grab_x, draw_br_y - grab_y);
        }
    }
    
    void drawDebug() {
        ofPushStyle();
        ofSetColor(255, 127);
        grabber.draw(0, 0, width, height);
        ofSetColor(255);
        cropped.draw(tl.x, tl.y, br.x-tl.x, br.y-tl.y);
        ofSetColor(highlightingTl ? ofColor(0, 255, 0) : ofColor(255, 0, 0));
        ofDrawCircle(tl.x, tl.y, mouseRadius);
        ofSetColor(highlightingBr ? ofColor(0, 255, 0) : ofColor(255, 0, 0));
        ofDrawCircle(br.x, br.y, mouseRadius);
        ofPopStyle();
    }
    
    void draw(int x = -1, int y = -1, int w = -1, int h = -1) {
        if (x == -1 || y == -1)
        {
            grabber.draw(0, 0, draw_br_x - grab_x, draw_br_y - grab_y);
        }
        else if (w == -1 || h == -1)
        {
            grabber.draw(x, y);
        }
        else
        {
            grabber.draw(x, y, w, h);
        }
    }

    void draw_text()
    {
        char str[256];
        ofSetColor(255);

        // Show sizes
        sprintf(str, "tl [%f %f] br [%f %f] br-tl [%f %f] wh [%d %d]", tl.x, tl.y, br.x, br.y, br.x - tl.x, br.y - tl.y, width, height);
        ofDrawBitmapString(str, 20, 20);

        // Show sizes
        sprintf(str, "screen_wh [%d %d] grab_xy [%f %f] draw_br_xy [%f %f] draw_br_xy - grab_xy [%f %f]", 
            screen_w, screen_h, grab_x, grab_y, draw_br_x, draw_br_y, draw_br_x - grab_x, draw_br_y - grab_y);
        ofDrawBitmapString(str, 20, 50);


        // Show fps
        sprintf(str, "fps: %3.3d", (int)ofGetFrameRate());
        ofDrawBitmapString(str, ofGetWidth() - 220, 20);
    }
    
    void mouseMoved(int x, int y){
        if (!debug || draggingTl || draggingBr || dragging) return;
        highlightingTl = false;
        highlightingBr = false;
        float dtl = ofDist(x, y, tl.x, tl.y);
        float dtr = ofDist(x, y, br.x, br.y);
        if (dtl < dtr && dtl < mouseRadius) {
            highlightingTl = true;
        } else if (dtr < dtl && dtr < mouseRadius) {
            highlightingBr = true;
        }
    }
    
    void mouseDragged(int x, int y){
        if (!debug) return;
        if (dragging) {
            float dx = x-anchor.x;
            float dy = y-anchor.y;
            tl.set(ofClamp(tl.x + dx, 0, width), ofClamp(tl.y + dy, 0, height));
            br.set(ofClamp(br.x + dx, 0, width), ofClamp(br.y + dy, 0, height));
            anchor.set(x, y);
        } else if (draggingTl) {
            tl.set(ofClamp(x, 0, width), ofClamp(y, 0, height));
        } else if (draggingBr) {
            br.set(ofClamp(x, 0, width), ofClamp(y, 0, height));
        }
    }
    
    void mousePressed(int x, int y){
        if (!debug) return;
        if (highlightingTl) {
            draggingTl = true;
        } else if (highlightingBr) {
            draggingBr = true;
        } else if (x > tl.x && x < br.x && y > tl.y && y < br.y) {
            dragging = true;
            anchor.set(x, y);
        }
    }
    
    void mouseReleased(int x, int y){
        if (!debug) return;
        draggingTl = false;
        draggingBr = false;
        dragging = false;
    }
    
    void setDebug(bool debug) {
        this->debug = debug;
        grabber.free();
        if (debug) {
            grabber.setup(ofGetScreenWidth(), ofGetScreenHeight(), retina);
        } else {
            grabber.setup(ofMap(br.x - tl.x, 0, width, 0, ofGetScreenWidth()),
                          ofMap(br.y - tl.y, 0, height, 0, ofGetScreenHeight()), retina);
        }
    }
    
    void toggleDebug() {setDebug(!isDebug());}
    
    bool isDebug() {return debug;}

private:
    
    ofxScreenGrab grabber;
    ofPixels pixels;
    ofImage cropped;
    ofPoint tl, br, anchor;
    bool highlightingTl, highlightingBr;
    bool draggingTl, draggingBr, dragging;
    int width, height;
    bool retina;
    float mouseRadius;
    bool debug;

};
