#include "ofxScreenGrab.h"

//-------------
void ofxScreenGrab::setup(int width, int height, bool retina) {
    this->width = width;
    this->height = height;
    if (retina) rx = 2;
    else        rx = 1;
    tex.allocate(rx * width, rx * height, GL_RGBA);

	screen_xi = -1;
	screen_yi = -1;


#ifdef _WIN32
	screen_context = GetDC( NULL );
	screen_x = this->width;// GetDeviceCaps( screen_context, HORZRES ); //(fullscreen)
	screen_y = this->height;// GetDeviceCaps( screen_context, VERTRES ); //(fullscreen)
	compatible_screen_context = CreateCompatibleDC( screen_context );
	screen_bitmap = CreateCompatibleBitmap( screen_context, screen_x, screen_y );
	hOld = SelectObject( compatible_screen_context, screen_bitmap );
	BitBlt( compatible_screen_context, 0, 0, screen_x, screen_y, screen_context, 0, 0, SRCCOPY );
	SelectObject( compatible_screen_context, hOld );

	//ReleaseDC(NULL, screen_context);
	DeleteDC(compatible_screen_context);
	DeleteObject(screen_bitmap);

	info;
	info.biSize = sizeof( BITMAPINFOHEADER );
	info.biPlanes = 1;
	info.biBitCount = 32;
	info.biWidth = screen_x;
	info.biHeight = -screen_y;
	info.biCompression = BI_RGB;
	info.biSizeImage = 0;


	fullscreen_x = ofGetScreenWidth(); //(fullscreen)
	fullscreen_y = ofGetScreenHeight(); //(fullscreen)

	screenPixels = new RGBQUAD[ fullscreen_x * fullscreen_y ];

	pPixels = new unsigned char[ fullscreen_x * fullscreen_y * 4 ]();

#endif
}

void ofxScreenGrab::free()
{
	if (screenPixels)
	{
		delete[] screenPixels;
		screenPixels = 0;
	}

	if (pPixels)
	{
		delete[] pPixels;
		pPixels = 0;
	}
}


//-------------
void ofxScreenGrab::grabScreen(int x, int y, int w, int h) {
    unsigned char * data = pixelsBelowWindow(x, y, w, h);
#ifndef _WIN32
    for (int i = 0; i < rx*rx*w*h; i++){
		unsigned char r1 = data[i*4];
		data[i*4]   = data[i*4+1];
		data[i*4+1] = data[i*4+2];
		data[i*4+2] = data[i*4+3];
		data[i*4+3] = r1;
	}	
	
#endif // !_WIN32
	if (data != NULL)
	{
		tex.loadData(data, rx*w, rx*h, GL_RGBA);
		tex.readToPixels(pixels);
	}
}

//-------------
void ofxScreenGrab::draw(int x, int y, int w, int h) {
    tex.draw(x, y, w, h);
}
#ifdef _WIN32
unsigned char * ofxScreenGrab::pixelsBelowWindow( int xi, int yi, int wi, int hi )
{
	bool reinit = !(screen_xi == xi && screen_yi == yi && screen_x == wi && screen_y == hi);

	if (reinit)
	{
		SelectObject(compatible_screen_context, hOld);
		//ReleaseDC( NULL, screen_context );
		DeleteDC(compatible_screen_context);
		DeleteObject(screen_bitmap);

		screen_xi = xi;
		screen_yi = yi;

		screen_x = wi;// GetDeviceCaps( screen_context, HORZRES ); //(fullscreen)
		screen_y = hi;// GetDeviceCaps( screen_context, VERTRES ); //(fullscreen)

		compatible_screen_context = CreateCompatibleDC( screen_context );
		screen_bitmap = CreateCompatibleBitmap( screen_context, screen_x, screen_y );
		hOld = SelectObject( compatible_screen_context, screen_bitmap );
	}

	//update
	//screen_context = GetDC( NULL );

	BitBlt( compatible_screen_context, 0, 0, screen_x, screen_y, screen_context, xi, yi, SRCCOPY );
	GetDIBits( compatible_screen_context, screen_bitmap, 0, screen_y, screenPixels, ( BITMAPINFO* ) &info, DIB_RGB_COLORS );

	// getimage
	for (int iy = 0; iy < hi; iy++){
		int iy_wi = iy*wi;
		for (int ix = 0; ix < wi; ix++) {
			int i = ix + iy_wi;
			int i4 = i*4;
			RGBQUAD p = screenPixels[i];
			pPixels[i4] = (unsigned char)(p.rgbRed);
			pPixels[i4 + 1] = (unsigned char)(p.rgbGreen);
			pPixels[i4 + 2] = (unsigned char)(p.rgbBlue);
			pPixels[i4 + 3] = (unsigned char)(255);
		}

	}
	return pPixels;
}
#endif